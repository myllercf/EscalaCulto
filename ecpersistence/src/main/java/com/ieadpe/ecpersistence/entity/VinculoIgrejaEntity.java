package com.ieadpe.ecpersistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="vinculo_igreja")
public class VinculoIgrejaEntity {
	
	@Id
	@SequenceGenerator(name="vinculo_igreja_id_seq", sequenceName="vinculo_igreja_id_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vinculo_igreja_id_seq")
	@Column(name = "id")
	private long id;
	
	@Column(name = "vinculo", nullable = false)
	private String vinculo;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVinculo() {
		return vinculo;
	}

	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}

}
