package com.ieadpe.ecpersistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="igreja")
public class CultoEntity {
	
	@Id
	@SequenceGenerator(name="culto_id_seq", sequenceName="culto_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "culto_id_seq")
	@Column(name = "id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "nome")
	@Transient
	private TipoCultoEntity nome;
	
	@Column(name = "frequencia", nullable = false)
	private String frequencia;
	
	@Column(name = "dia", nullable = false)
	private String dia;
	
	@Column(name = "turno", nullable = false)
	private String turno;
	
	@ManyToOne
	@JoinColumn(name = "dirigente")
	@Transient
	private PessoaEntity dirigente;
	
	@Column(name = "tem_dirigente", nullable = false)
	private char temDirigente;
	
	@ManyToOne
	@JoinColumn(name = "igreja")
	@Transient
	private IgrejaEntity igreja;
	
	@Column(name = "observacoes", nullable = true)
	private String observacoes;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TipoCultoEntity getNome() {
		return nome;
	}

	public void setNome(TipoCultoEntity nome) {
		this.nome = nome;
	}

	public String getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(String frequencia) {
		this.frequencia = frequencia;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public PessoaEntity getDirigente() {
		return dirigente;
	}

	public void setDirigente(PessoaEntity dirigente) {
		this.dirigente = dirigente;
	}

	public char getTemDirigente() {
		return temDirigente;
	}

	public void setTemDirigente(char temDirigente) {
		this.temDirigente = temDirigente;
	}

	public IgrejaEntity getIgreja() {
		return igreja;
	}

	public void setIgreja(IgrejaEntity igreja) {
		this.igreja = igreja;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

}
