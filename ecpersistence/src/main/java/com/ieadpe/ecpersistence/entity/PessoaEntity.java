package com.ieadpe.ecpersistence.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pessoa")
public class PessoaEntity {
	
	@Id
	@SequenceGenerator(name="pessoa_id_seq", sequenceName="pessoa_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_id_seq")
	@Column(name = "id")
	private long id;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Column(name = "sobrenome", nullable = false)
	private String sobrenome;
	
	@ManyToOne
	@JoinColumn(name = "id_estado_civil")
	@Transient
	private EstadoCivilEntity estadoCivil;
	
	@ManyToOne
	@JoinColumn(name = "id_vinculo_igreja")
	@Transient
	private VinculoIgrejaEntity vinculo;
	
	@Column(name = "cartao_membro")
	private String cartaoMembro;
	
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "data_batismo")
	private Date dataBatismo;
	
	@Column(name = "foto")
	private byte[] foto;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "id_endereco")
	private EnderecoEntity endereco;
	
	@ManyToOne
	@JoinColumn(name = "congrega")
	@Transient
	private IgrejaEntity congregacao;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public EstadoCivilEntity getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilEntity estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public VinculoIgrejaEntity getVinculo() {
		return vinculo;
	}

	public void setVinculo(VinculoIgrejaEntity vinculo) {
		this.vinculo = vinculo;
	}

	public String getCartaoMembro() {
		return cartaoMembro;
	}

	public void setCartaoMembro(String cartaoMembro) {
		this.cartaoMembro = cartaoMembro;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataBatismo() {
		return dataBatismo;
	}

	public void setDataBatismo(Date dataBatismo) {
		this.dataBatismo = dataBatismo;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public EnderecoEntity getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoEntity endereco) {
		this.endereco = endereco;
	}

	public IgrejaEntity getCongregacao() {
		return congregacao;
	}

	public void setCongregacao(IgrejaEntity congregacao) {
		this.congregacao = congregacao;
	}

}
