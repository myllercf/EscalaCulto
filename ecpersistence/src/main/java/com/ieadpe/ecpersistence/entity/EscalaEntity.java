package com.ieadpe.ecpersistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="igreja")
public class EscalaEntity {
	
	@Id
	@SequenceGenerator(name="escala_id_seq", sequenceName="escala_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "escala_id_seq")
	@Column(name = "id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "culto")
	@Transient
	private CultoEntity culto;
	
	@ManyToOne
	@JoinColumn(name = "escalado")
	@Transient
	private PessoaEntity escalado;
	
	@Column(name = "data", nullable = false)
	private Date data;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CultoEntity getCulto() {
		return culto;
	}

	public void setCulto(CultoEntity culto) {
		this.culto = culto;
	}

	public PessoaEntity getEscalado() {
		return escalado;
	}

	public void setEscalado(PessoaEntity escalado) {
		this.escalado = escalado;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
