package com.ieadpe.ecpersistence.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="igreja")
public class IgrejaEntity {
	
	@Id
	@SequenceGenerator(name="generator_igreja", sequenceName="igreja_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_igreja")
	@Column(name = "id")
	private long id;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Lob
	@Column(name = "foto", nullable = true)
	private byte[] foto;
	
	@ManyToOne
	@JoinColumn(name = "id_tipo_templo")
	@Transient
	private TipoTemploEntity tipoTemplo;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "id_endereco")
	private EnderecoEntity endereco;
	
	@ManyToOne
	@JoinColumn(name = "responsavel")
	@Transient
	private PessoaEntity responsavel;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public TipoTemploEntity getTipoTemplo() {
		return tipoTemplo;
	}

	public void setTipoTemplo(TipoTemploEntity tipoTemplo) {
		this.tipoTemplo = tipoTemplo;
	}

	public EnderecoEntity getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoEntity endereco) {
		this.endereco = endereco;
	}

	public PessoaEntity getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(PessoaEntity responsavel) {
		this.responsavel = responsavel;
	}

}
