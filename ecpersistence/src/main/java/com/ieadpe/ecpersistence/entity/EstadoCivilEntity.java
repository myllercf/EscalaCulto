package com.ieadpe.ecpersistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="estado_civil")
public class EstadoCivilEntity {
	
	@Id
	@SequenceGenerator(name="estado_civil_id_seq", sequenceName="estado_civil_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estado_civil_id_seq")
	@Column(name = "id")
	private long id;
	
	@Column(name = "estado_civil", nullable = false)
	private String estadoCivil;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

}
