package com.ieadpe.ecpersistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="tipo_templo")
public class TipoTemploEntity {
	
	@Id
	@SequenceGenerator(name="tipo_templo_id_seq", sequenceName="tipo_templo_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_templo_id_seq")
	@Column(name = "id")
	private long id;
	
	@Column(name = "classificacao", nullable = false)
	private String classificacao;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}

}
