package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.EnderecoEntity;

@Repository
public interface IEnderecoRepository extends CrudRepository<EnderecoEntity, Long> {

}
