package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.PessoaEntity;

@Repository
public interface IPessoaRepository extends CrudRepository<PessoaEntity, Long>{

}
