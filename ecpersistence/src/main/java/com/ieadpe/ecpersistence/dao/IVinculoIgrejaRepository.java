package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.VinculoIgrejaEntity;

@Repository
public interface IVinculoIgrejaRepository extends CrudRepository<VinculoIgrejaEntity, Long>{

}
