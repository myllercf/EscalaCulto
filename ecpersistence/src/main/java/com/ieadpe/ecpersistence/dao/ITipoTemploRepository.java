package com.ieadpe.ecpersistence.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.TipoTemploEntity;

@Repository
public interface ITipoTemploRepository extends CrudRepository<TipoTemploEntity, Long>{
	
	List<TipoTemploEntity> findAll();
	
	//public void salvar(TipoTemploEntity tipoTemplo) throws DaoException;
	
//	@Query
//	public List<TipoTemploEntity> listarTipoTemplo() throws DaoException;
	
	//@Query
	//List<TipoTemploEntity> findByClassificacao(String classificacao) throws DaoException;

}
