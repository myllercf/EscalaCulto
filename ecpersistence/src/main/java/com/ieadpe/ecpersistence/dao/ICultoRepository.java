package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.CultoEntity;

@Repository
public interface ICultoRepository extends CrudRepository<CultoEntity, Long>{

}
