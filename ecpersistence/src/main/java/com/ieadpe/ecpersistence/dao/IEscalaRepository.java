package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.EscalaEntity;

@Repository
public interface IEscalaRepository extends CrudRepository<EscalaEntity, Long>{

}
