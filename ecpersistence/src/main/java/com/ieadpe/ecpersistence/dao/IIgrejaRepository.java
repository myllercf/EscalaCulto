package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;

import com.ieadpe.ecpersistence.entity.IgrejaEntity;

public interface IIgrejaRepository extends CrudRepository<IgrejaEntity, Long>{

}
