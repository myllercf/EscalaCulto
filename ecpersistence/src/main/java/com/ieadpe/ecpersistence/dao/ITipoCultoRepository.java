package com.ieadpe.ecpersistence.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ieadpe.ecpersistence.entity.TipoCultoEntity;

@Repository
public interface ITipoCultoRepository extends CrudRepository<TipoCultoEntity, Long>{

}
