package com.ieadpe.ecpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcpersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcpersistenceApplication.class, args);
	}
}
