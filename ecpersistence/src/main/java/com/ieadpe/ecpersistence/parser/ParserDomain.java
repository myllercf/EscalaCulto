package com.ieadpe.ecpersistence.parser;

import com.ieadpe.ecbusiness.domain.Coordenadas;
import com.ieadpe.ecbusiness.domain.Endereco;
import com.ieadpe.ecbusiness.domain.EstadoCivil;
import com.ieadpe.ecbusiness.domain.TipoCulto;
import com.ieadpe.ecbusiness.domain.TipoTemplo;
import com.ieadpe.ecbusiness.domain.VinculoIgreja;
import com.ieadpe.ecbusiness.exception.ParserException;
import com.ieadpe.ecpersistence.entity.EnderecoEntity;
import com.ieadpe.ecpersistence.entity.EstadoCivilEntity;
import com.ieadpe.ecpersistence.entity.TipoCultoEntity;
import com.ieadpe.ecpersistence.entity.TipoTemploEntity;
import com.ieadpe.ecpersistence.entity.VinculoIgrejaEntity;

public class ParserDomain {
	
	public static Endereco parserEnderecoDomain (EnderecoEntity enderecoEntity) throws ParserException {
		
		if(null == enderecoEntity) {
			throw new ParserException("Falha ao converter endereço entity para domain, objeto nulo.");
		}
		
		Endereco endereco = new Endereco();
		endereco.setId(enderecoEntity.getId());
		endereco.setRua(enderecoEntity.getRua());
		endereco.setNumero(enderecoEntity.getNumero());
		endereco.setCep(enderecoEntity.getCep());
		endereco.setBairro(enderecoEntity.getBairro());
		endereco.setCidade(enderecoEntity.getCidade());
		endereco.setComplemento(enderecoEntity.getComplemento());
		
		Coordenadas coordenadas = parserCoordenadas(enderecoEntity);		
		endereco.setCoordenadas(coordenadas);
		
		return endereco;
	}

	private static Coordenadas parserCoordenadas(EnderecoEntity enderecoEntity) {
		Coordenadas coordenadas = new Coordenadas();
		coordenadas.setLatitude(enderecoEntity.getLatitude());
		coordenadas.setLongitude(enderecoEntity.getLongitude());
		return coordenadas;
	}
	
	public static TipoTemplo parserTipoTemploDomain(TipoTemploEntity tipoTemploEntity) throws ParserException {
		
		if(null == tipoTemploEntity) {
			throw new ParserException("Falha ao converter tipo templo entity para domain, objeto nulo.");
		}
		
		TipoTemplo tipoTemplo = new TipoTemplo();
		
		tipoTemplo.setId(tipoTemploEntity.getId());
		tipoTemplo.setClassificacao(tipoTemploEntity.getClassificacao());
		
		return tipoTemplo;
	}
	
	public static TipoCulto parserTipoCultoDomain(TipoCultoEntity tipoCultoEntity) throws ParserException {
		
		if(null == tipoCultoEntity) {
			throw new ParserException("Falha ao converter tipode culto entity para domain, objeto nulo.");
		}
		
		TipoCulto tipoCulto = new TipoCulto();
		
		tipoCulto.setId(tipoCultoEntity.getId());
		tipoCulto.setClassificacao(tipoCultoEntity.getClassificacao());
		
		return tipoCulto;
	}
	
	public static EstadoCivil parserEstadoCivilDomain(EstadoCivilEntity estadoCivilEntity) throws ParserException {
		
		if(null == estadoCivilEntity) {
			throw new ParserException("Falha ao converter estado civil entity para domain, objeto nulo.");
		}
		EstadoCivil estadoCivil = new EstadoCivil();
		
		estadoCivil.setId(estadoCivilEntity.getId());
		estadoCivil.setEstadoCivil(estadoCivilEntity.getEstadoCivil());
		
		return estadoCivil;
	}
	
	public static VinculoIgreja parserVinculoIgrejaDomain(VinculoIgrejaEntity vinculoIgrejaEntity) throws ParserException {
		
		if(null == vinculoIgrejaEntity) {
			throw new ParserException("Falha ao converter vínculo igreja entity para domain, objeto nulo.");
		}
		
		VinculoIgreja vinculoIgreja = new VinculoIgreja();
		
		vinculoIgreja.setId(vinculoIgrejaEntity.getId());
		vinculoIgreja.setVinculo(vinculoIgrejaEntity.getVinculo());
		
		return vinculoIgreja;
	}

}
