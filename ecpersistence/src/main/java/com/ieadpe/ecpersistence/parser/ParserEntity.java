package com.ieadpe.ecpersistence.parser;

import com.ieadpe.ecbusiness.domain.Culto;
import com.ieadpe.ecbusiness.domain.Endereco;
import com.ieadpe.ecbusiness.domain.Escala;
import com.ieadpe.ecbusiness.domain.EstadoCivil;
import com.ieadpe.ecbusiness.domain.Igreja;
import com.ieadpe.ecbusiness.domain.Pessoa;
import com.ieadpe.ecbusiness.domain.TipoCulto;
import com.ieadpe.ecbusiness.domain.TipoTemplo;
import com.ieadpe.ecbusiness.domain.VinculoIgreja;
import com.ieadpe.ecbusiness.exception.ParserException;
import com.ieadpe.ecpersistence.entity.CultoEntity;
import com.ieadpe.ecpersistence.entity.EnderecoEntity;
import com.ieadpe.ecpersistence.entity.EscalaEntity;
import com.ieadpe.ecpersistence.entity.EstadoCivilEntity;
import com.ieadpe.ecpersistence.entity.IgrejaEntity;
import com.ieadpe.ecpersistence.entity.PessoaEntity;
import com.ieadpe.ecpersistence.entity.TipoCultoEntity;
import com.ieadpe.ecpersistence.entity.TipoTemploEntity;
import com.ieadpe.ecpersistence.entity.VinculoIgrejaEntity;

//TODO falta tratar a redundancia dos parsers
public class ParserEntity {
	
	public static EnderecoEntity parserEnderecoEntity(Endereco endereco) throws ParserException {
		
		if(null == endereco) {
			throw new ParserException("Falha ao converter endereço para entity, objeto nulo.");
		}
		EnderecoEntity enderecoEntity = new EnderecoEntity();
		
		enderecoEntity.setId(endereco.getId());
		enderecoEntity.setRua(endereco.getRua());
		enderecoEntity.setNumero(endereco.getNumero());
		enderecoEntity.setCep(endereco.getCep());
		enderecoEntity.setBairro(endereco.getBairro());
		enderecoEntity.setCidade(endereco.getCidade());
		enderecoEntity.setComplemento(endereco.getComplemento());
		enderecoEntity.setLatitude(endereco.getCoordenadas().getLatitude());
		enderecoEntity.setLongitude(endereco.getCoordenadas().getLongitude());
		
		return enderecoEntity;
	}
	
	public static TipoTemploEntity parserTipoTemploEntity(TipoTemplo tipoTemplo) throws ParserException {
		
		if(null == tipoTemplo) {
			throw new ParserException("Falha ao converter tipo de templo para entity, objeto nulo.");
		}
		
		TipoTemploEntity tipoTemploEntity = new TipoTemploEntity();
		
		tipoTemploEntity.setId(tipoTemplo.getId());
		tipoTemploEntity.setClassificacao(tipoTemplo.getClassificacao());
		
		return tipoTemploEntity;
	}
	
	public static TipoCultoEntity parserTipoCultoEntity(TipoCulto tipoCulto) throws ParserException {
		
		if(null == tipoCulto) {
			throw new ParserException("Falha ao converter tipode culto para entity, objeto nulo.");
		}
		
		TipoCultoEntity tipoCultoEntity = new TipoCultoEntity();
		
		tipoCultoEntity.setId(tipoCulto.getId());
		tipoCultoEntity.setClassificacao(tipoCulto.getClassificacao());
		
		return tipoCultoEntity;
	}
	
	public static EstadoCivilEntity parserEstadoCivilEntity(EstadoCivil estadoCivil) throws ParserException {
		
		if(null == estadoCivil) {
			throw new ParserException("Falha ao converter estado civil para entity, objeto nulo.");
		}
		EstadoCivilEntity estadoCivilEntity = new EstadoCivilEntity();
		
		estadoCivilEntity.setId(estadoCivil.getId());
		estadoCivilEntity.setEstadoCivil(estadoCivil.getEstadoCivil());
		
		return estadoCivilEntity;
	}
	
	public static VinculoIgrejaEntity parserVinculoIgrejaEntity (VinculoIgreja vinculoIgreja) throws ParserException {
		
		if(null == vinculoIgreja) {
			throw new ParserException("Falha ao converter vínculo igreja para entity, objeto nulo.");
		}
		
		VinculoIgrejaEntity vinculoIgrejaEntity = new VinculoIgrejaEntity();
		
		vinculoIgrejaEntity.setId(vinculoIgreja.getId());
		vinculoIgrejaEntity.setVinculo(vinculoIgreja.getVinculo());
		
		return vinculoIgrejaEntity;
	}
	
	public static PessoaEntity parserPessoaEntity(Pessoa pessoa) throws ParserException {
		
		if(null == pessoa) {
			throw new ParserException("Falha ao converter pessoa para entity, objeto nulo.");
		}
		
		PessoaEntity pessoaEntity = new PessoaEntity();
		
		pessoaEntity.setId(pessoa.getId());
		pessoaEntity.setNome(pessoa.getNome());
		pessoaEntity.setSobrenome(pessoa.getSobrenome());
		
		EstadoCivilEntity estadoCivilEntity = new EstadoCivilEntity();
		estadoCivilEntity = parserEstadoCivilEntity(pessoa.getEstadoCivil());
		pessoaEntity.setEstadoCivil(estadoCivilEntity);
		
		VinculoIgrejaEntity vinculoIgrejaEntity = new VinculoIgrejaEntity();
		vinculoIgrejaEntity = parserVinculoIgrejaEntity(pessoa.getVinculo());
		pessoaEntity.setVinculo(vinculoIgrejaEntity);
		
		pessoaEntity.setCartaoMembro(pessoa.getCartaoMembro());
		pessoaEntity.setDataNascimento(pessoa.getDataNascimento());
		pessoaEntity.setDataBatismo(pessoa.getDataBatismo());
		pessoaEntity.setFoto(pessoa.getFoto());
		
		EnderecoEntity enderecoEntity = parserEnderecoEntity(pessoa.getEndereco());
		pessoaEntity.setEndereco(enderecoEntity);
		
		//tratar
		IgrejaEntity igrejaEntity = parserIgrejaEntity(pessoa.getCongregacao());
		//IgrejaEntity igrejaEntity = parserIgrejaEntity(pessoaEntity, pessoa.getCongregacao());
		pessoaEntity.setCongregacao(igrejaEntity);
		
		return pessoaEntity;
	}
	
//	private static IgrejaEntity parserIgrejaEntity(PessoaEntity pessoaEntity, Igreja congregacao) throws ParserException {
//		
//		if(null == congregacao) {
//			throw new ParserException("Falha ao converter congregação para entity, objeto nulo.");
//		}
//		
//		IgrejaEntity igrejaEntity = new IgrejaEntity();
//		
//		igrejaEntity.setId(congregacao.getId());
//		igrejaEntity.setNome(congregacao.getNome());
//		igrejaEntity.setFoto(congregacao.getFoto());
//		
//		igrejaEntity.setResponsavel(pessoaEntity);
//		
//		return igrejaEntity;
//	}

	public static IgrejaEntity parserIgrejaEntity(Igreja igreja) throws ParserException {
		
		if(null == igreja) {
			throw new ParserException("Falha ao converter igreja para entity, objeto nulo.");
		}
		
		IgrejaEntity igrejaEntity = new IgrejaEntity();
		
		igrejaEntity.setId(igreja.getId());
		igrejaEntity.setNome(igreja.getNome());
		igrejaEntity.setFoto(igreja.getFoto());
		
		TipoTemploEntity tipoTemploEntity = new TipoTemploEntity();
		tipoTemploEntity = parserTipoTemploEntity(igreja.getTipoTemplo());
		igrejaEntity.setTipoTemplo(tipoTemploEntity);
		
		EnderecoEntity enderecoEntity = new EnderecoEntity();
		enderecoEntity = parserEnderecoEntity(igreja.getEndereco());
		igrejaEntity.setEndereco(enderecoEntity);
		
		PessoaEntity pessoaEntity = new PessoaEntity();
		pessoaEntity = parserPessoaEntity(igreja.getResponsavel());
		igrejaEntity.setResponsavel(pessoaEntity);
		
		return igrejaEntity;
	}
	
	public static CultoEntity parserCultoEntity(Culto culto) throws ParserException {
		
		if(null == culto) {
			throw new ParserException("Falha ao converter culto para entity, objeto nulo.");
		}
		
		CultoEntity cultoEntity = new CultoEntity();
		
		cultoEntity.setId(culto.getId());
		
		TipoCultoEntity tipoCultoEntity = new TipoCultoEntity();
		tipoCultoEntity = parserTipoCultoEntity(culto.getNome());
		cultoEntity.setNome(tipoCultoEntity);
		
		cultoEntity.setFrequencia(culto.getFrequencia());
		cultoEntity.setDia(culto.getDia());
		cultoEntity.setTurno(culto.getTurno());
		
		PessoaEntity pessoaEntity = new PessoaEntity();
		pessoaEntity = parserPessoaEntity(culto.getDirigente());
		cultoEntity.setDirigente(pessoaEntity);
		
		cultoEntity.setTemDirigente(culto.getTemDirigente());
		
		IgrejaEntity igrejaEntity = new IgrejaEntity();
		igrejaEntity = parserIgrejaEntity(culto.getIgreja());
		cultoEntity.setIgreja(igrejaEntity);
		
		cultoEntity.setObservacoes(culto.getObservacoes());
		
		return cultoEntity;
	}
	
	public static EscalaEntity parserEscalaEntity(Escala escala) throws ParserException {
		
		if(null == escala) {
			throw new ParserException("Falha ao converter escala para entity, objeto nulo.");
		}
		
		EscalaEntity escalaEntity = new EscalaEntity();
		
		escalaEntity.setId(escala.getId());
		
		CultoEntity cultoEntity = new CultoEntity();
		cultoEntity = parserCultoEntity(escala.getCulto());
		escalaEntity.setCulto(cultoEntity);
		
		PessoaEntity pessoaEntity = new PessoaEntity();
		pessoaEntity = parserPessoaEntity(escala.getEscalado());
		escalaEntity.setEscalado(pessoaEntity);
		
		escalaEntity.setData(escala.getData());
		
		return escalaEntity;
	}

}
