CREATE USER coordenador SUPERUSER INHERIT CREATEDB CREATEROLE;
ALTER USER coordenador PASSWORD 'senha'; 

-- Database: escala_culto

-- DROP DATABASE escala_culto;

CREATE DATABASE escala_culto
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'pt_BR.UTF-8'
       LC_CTYPE = 'pt_BR.UTF-8'
       CONNECTION LIMIT = -1;

COMMENT ON DATABASE escala_culto
  IS 'base de dados do projeto de escala de cultos';



--tabelas

create table tipo_templo
(
	id serial PRIMARY KEY
	, classificacao varchar(20) not null
);

create table estado_civil
(
	id serial primary key
	, estado_civil varchar(15)
);

create table vinculo_igreja
(
	id serial primary key
	, vinculo varchar(20)
);

create table endereco
(
	id serial primary key
	, rua varchar(50) not null
	, numero varchar(10)
	, cep varchar(10)
	, bairro varchar(20) not null
	, cidade varchar(50) not null
	, complemento text
	, latitude decimal(11,8)
	, longitude decimal(11,8)
);

CREATE TABLE pessoa
(
	id serial primary key
	, nome varchar(20) not null
	, sobrenome varchar(30) not null
	, id_estado_civil integer references estado_civil (id)
	, id_vinculo_igreja integer references vinculo_igreja (id)
	, cartao_membro varchar(9)
	, data_nascimento date
	, data_batismo date
	, foto bytea
	, id_endereco integer references endereco (id)
	--, igreja integer references igreja (id)
);


create table igreja
(
	id serial primary key
	, nome varchar(30) not null
	--, endereco text not null
	, foto bytea
	, id_tipo_templo integer references tipo_templo (id)
	, id_endereco integer references endereco (id)
);

alter table pessoa
add congrega integer references igreja (id);

alter table igreja
add responsavel integer references pessoa (id);


create table tipo_culto
(
	id serial primary key
	, classificacao varchar(25) not null
);


--create table ocorrencia(id serial primary key, classificacao varchar(15) not null);


create type ocorrencia as enum ('diário', 'semanal', 'mensal', 'esporádico', 'anual');
create type dias as enum ('domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado');
create type turnos as enum ('manha', 'tarde', 'noite');


create type tem_dirigente as enum ('S', 'N');
create table culto
(
	id serial primary key
	, nome integer references tipo_culto (id)
	, frequencia ocorrencia not null
	, dia dias not null
	, turno turnos not null
	, dirigente integer references pessoa (id)
	, tem_dirigente tem_dirigente not null
	, igreja integer references igreja (id)
	, observacoes text
);


create table escala
(
	id serial primary key
	, culto integer references culto (id)
	, escalado integer references pessoa (id)
	, data date not null
);


CREATE OR REPLACE FUNCTION verificar_escalado() RETURNS trigger AS $verificar_escalado$
    BEGIN
        -- Check that empname and salary are given
        IF (
		(SELECT tem_dirigente FROM culto WHERE id = NEW.culto) = 'S'
		AND
		(SELECT dirigente FROM culto WHERE id = NEW.culto) <> NEW.escalado
	) THEN
		RAISE EXCEPTION 'Este culto possui dirigente';
	END IF;
        RETURN NEW;
    END;
$verificar_escalado$ LANGUAGE plpgsql;
--drop
CREATE TRIGGER verificar_escalado BEFORE INSERT OR UPDATE ON escala
    FOR EACH ROW EXECUTE PROCEDURE verificar_escalado();


create table obreiro_culto
(
	obreiro integer references pessoa (id) not null
	, culto integer references tipo_culto not null
);


--SEQUENCE
--tipo_templo
CREATE SEQUENCE s_tipo_templo START 1;