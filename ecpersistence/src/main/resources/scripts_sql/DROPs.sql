CREATE USER coordenador SUPERUSER INHERIT CREATEDB CREATEROLE;
ALTER USER coordenador PASSWORD 'senha';

-- Database: escala_culto

--tabelas

drop table tipo_templo;
drop table estado_civil;
drop table vinculo_igreja;
drop table endereco;
drop TABLE pessoa;
drop table igreja;

alter table pessoa drop column congrega;
alter table igreja drop column responsavel;

drop table tipo_culto;

drop type ocorrencia;
drop type dias;
drop type turnos;

drop type tem_dirigente;

drop table culto;
DROP TRIGGER verificar_escalado ON escala;
drop table escala;

drop function verificar_escalado();
drop trigger verificar_escalado();

drop table obreiro_culto

drop SEQUENCE s_tipo_templo;

DROP DATABASE escala_culto;