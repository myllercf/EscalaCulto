insert into tipo_templo (classificacao) values ('congregação');
insert into tipo_templo (classificacao) values ('ponto de pregação');
insert into tipo_templo (classificacao) values ('igreja polo');
insert into tipo_templo (classificacao) values ('templo matriz');
insert into tipo_templo (classificacao) values ('templo central');

insert into estado_civil (estado_civil) values ('solteiro(a)');
insert into estado_civil (estado_civil) values ('casado(a)');
insert into estado_civil (estado_civil) values ('viúvo(a)');
insert into estado_civil (estado_civil) values ('divorciado(a)');
insert into estado_civil (estado_civil) values ('separado(a)');
insert into estado_civil (estado_civil) values ('união estável');

insert into vinculo_igreja (vinculo) values ('congregado');
insert into vinculo_igreja (vinculo) values ('membro');
insert into vinculo_igreja (vinculo) values ('auxiliar local');
insert into vinculo_igreja (vinculo) values ('auxiliar oficial');
insert into vinculo_igreja (vinculo) values ('diácono');
insert into vinculo_igreja (vinculo) values ('presbítero');
insert into vinculo_igreja (vinculo) values ('evangelista');
insert into vinculo_igreja (vinculo) values ('pastor auxiliar');
insert into vinculo_igreja (vinculo) values ('pastor presidente');

insert into pessoa(nome, sobrenome, id_estado_civil, id_vinculo_igreja, cartao_membro, data_nascimento, foto, data_batismo, congrega)
values ('Myller', 'Claudino de Freitas', 1, 2, '001508034', '13/02/1987', null, '28/06/2009', 1);

insert into igreja (nome, endereco, foto, latitude, longitude, id_tipo_templo)
values ('Boa viagem', 'R. Barão de Souza Leão, 1648 - Boa Viagem, Recife - PE, 51210-080', null, -8.132481, -34.914951, 1);

insert into tipo_culto (classificacao) values ('escola dominical');
insert into tipo_culto (classificacao) values ('evangelismo');
insert into tipo_culto (classificacao) values ('culto evangelístico');
insert into tipo_culto (classificacao) values ('culto missões');
insert into tipo_culto (classificacao) values ('culto mocidade');
insert into tipo_culto (classificacao) values ('culto proati');

insert into tipo_culto (classificacao) values ('culto jovem');
insert into tipo_culto (classificacao) values ('culto família');

insert into tipo_culto (classificacao) values ('círculo oração');
insert into tipo_culto (classificacao) values ('consagração');
insert into tipo_culto (classificacao) values ('matutino');
insert into tipo_culto (classificacao) values (‘cruzada’);

insert into tipo_culto (classificacao) values ('oração');
insert into tipo_culto (classificacao) values ('oração mocidade');
insert into tipo_culto (classificacao) values ('doutrina');
insert into tipo_culto (classificacao) values ('pregação');

insert into ocorrencia (classificacao) values('diário');
insert into ocorrencia (classificacao) values('semanal');
insert into ocorrencia (classificacao) values('mensal');
insert into ocorrencia (classificacao) values('esporádico');

insert into culto (nome, frequencia, dia, turno, dirigente, tem_dirigente, igreja, observacoes)
values (1, 'semanal', 'domingo', 'manha', 2, 'S', 1, 'teste de insert de culto. para este o dirigente é o irmão Carlos Alberto');

insert into escala (culto, escalado, data)
values(1, 2, '22-04-2017');
insert into escala (culto, escalado, data)
values(1, 1, '22-04-2017');

--Manipulacao nos testes
insert into tipo_templo (id, classificacao) values (1,'congregação');
insert into tipo_templo (id, classificacao) values (2,'ponto de pregação');
insert into tipo_templo (id, classificacao) values (3,'igreja polo');
insert into tipo_templo (id, classificacao) values (4,'templo matriz');
insert into tipo_templo (id, classificacao) values (5,'templo central');

select * from igreja;
select * from endereco;
delete from igreja
