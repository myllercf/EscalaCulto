package com.ieadpe.ecpersistence.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ieadpe.ecpersistence.dao.IIgrejaRepository;
import com.ieadpe.ecpersistence.entity.EnderecoEntity;
import com.ieadpe.ecpersistence.entity.IgrejaEntity;
import com.ieadpe.ecpersistence.entity.TipoTemploEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class IgrejaDAOTest {
	
	private static final Logger logger = Logger.getLogger(IgrejaDAOTest.class);
	
	@Autowired
	IIgrejaRepository igrejaRepository;
	
	@Test
	public void salvarIgrejaOK() {
		logger.info("inicio teste salvarIgrejaOK");
		
		IgrejaEntity igrejaEntity = new IgrejaEntity();
		igrejaEntity.setNome("Boa viagem");
		
		EnderecoEntity enderecoEntity = new EnderecoEntity();
		enderecoEntity.setRua("Rua São Francisco de Assis");
		enderecoEntity.setNumero("87");
		enderecoEntity.setCep("51030-520");
		enderecoEntity.setBairro("Boa viagem");
		enderecoEntity.setCidade("Recife");
		enderecoEntity.setComplemento("próximo ao Tio Pepe");
		enderecoEntity.setLatitude(-8.134224);
		enderecoEntity.setLongitude(-34.9095392);		
		igrejaEntity.setEndereco(enderecoEntity);
		
		TipoTemploEntity tipoTemploEntity = new TipoTemploEntity();
		tipoTemploEntity.setClassificacao("congregação");
		igrejaEntity.setTipoTemplo(tipoTemploEntity);
		
		BufferedImage originalImage = null;
		try {
			originalImage = ImageIO.read(new File("/home/myller/Imagens/Iron-Man-I-II_06.jpg"));
		} catch (IOException e) {
			Assert.fail("Falha ao salvar igreja");
		}
		ByteArrayOutputStream arrayBytes = new ByteArrayOutputStream();
		try {
			ImageIO.write( originalImage, "jpg", arrayBytes);
		} catch (IOException e) {
			Assert.fail("Falha ao salvar igreja");
		}
		try {
			arrayBytes.flush();
		} catch (IOException e) {
			Assert.fail("Falha ao salvar igreja");
		}
		byte[] foto = arrayBytes.toByteArray();
		
		igrejaEntity.setFoto(foto);
		
		List<IgrejaEntity> listaIgrejaEntity = new ArrayList<IgrejaEntity>();
		
		try {
			igrejaRepository.save(igrejaEntity);			 
			listaIgrejaEntity.addAll( (Collection<? extends IgrejaEntity>) igrejaRepository.findAll() );
		} catch (Exception e) {
			Assert.fail("Falha ao salvar igreja");
		}
		
		Assert.assertTrue("Persistencia de igreja realizada com sucesso", !listaIgrejaEntity.isEmpty());
		
		logger.info("fim teste salvarIgrejaOK");
	}

}
