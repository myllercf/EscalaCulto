package com.ieadpe.ecpersistence.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ieadpe.ecpersistence.dao.ITipoTemploRepository;
import com.ieadpe.ecpersistence.entity.TipoTemploEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
//@Transactional
public class TipoTemploDAOTest {
	
	private static final Logger logger = Logger.getLogger(TipoTemploDAOTest.class);
	
	@Autowired
	ITipoTemploRepository tipoTemploDAO;
	
	@Test
	@Ignore
	public void salvarTipoTemploOK() {
		logger.info("inicio teste salvarTipoTemploOK");
		
		TipoTemploEntity tipoTemplo = new TipoTemploEntity();
		tipoTemplo.setClassificacao("congregação");
		//tipoTemplo.setId(3);
		
		tipoTemploDAO.save(tipoTemplo);
		
		tipoTemplo = new TipoTemploEntity();
		tipoTemplo.setClassificacao("xxxxxx");
		tipoTemploDAO.save(tipoTemplo);
		
		Assert.assertTrue("TipoTemplo salvo com sucesso", true);
		
		logger.info("fim teste salvarTipoTemploOK");
	}
	
	@Test
	public void findAllOK() {
		logger.info("inicio teste findAllOK");
		
		List<TipoTemploEntity> lista = new ArrayList<TipoTemploEntity>();
		
		lista.addAll(tipoTemploDAO.findAll());
		
		if(lista.isEmpty()) {
			Assert.fail("Falha ao listar TipoTemplo");
		} else {
			Assert.assertTrue("Lista consultada com sucesso", true);
		}
		
		logger.info("fim teste findAllOK");
	}
	
	
//	@Test
//	public void findByClassificacaoOK() {
//		logger.info("inicio teste salvarTipoTemploOK");
//		
//		List<TipoTemploEntity> lista = new ArrayList<TipoTemploEntity>();
//		
//		try {
//			lista.addAll(tipoTemploDAO.findByClassificacao("evangelismo"));
//			//lista = tipoTemploDAO.findByClassificacao("evangelismo");
//		} catch (DaoException e) {
//			logger.error("deu pau", e);
//			Assert.fail("Falha ao salvar TipoTemplo");
//		}
//		
//		if(lista.isEmpty()) {
//			Assert.fail("Falha ao listar TipoTemplo");
//		} else {
//			Assert.assertTrue("Lista consultada com sucesso", true);
//		}
//		
//		logger.info("fim teste salvarTipoTemploOK");
//	}
	
	
//	@Test
//	public void salvarTipoTemploOK() {
//		logger.info("inicio teste salvarTipoTemploOK");
//		
//		TipoTemploEntity tipoTemplo = new TipoTemploEntity();
//		tipoTemplo.setClassificacao("classificacao teste");
//		
//		TipoTemploDAO tipoTemploDAO = new TipoTemploDAO();
//		try {
//			tipoTemploDAO.salvar(tipoTemplo);
//		} catch (DaoException e) {
//			Assert.fail("Falha ao salvar TipoTemplo");
//		}
//		
//		Assert.assertTrue("TipoTemplo salvo com sucesso", true);
//		
//		logger.info("fim teste salvarTipoTemploOK");
//	}

}
