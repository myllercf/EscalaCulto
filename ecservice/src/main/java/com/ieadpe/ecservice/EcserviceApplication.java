package com.ieadpe.ecservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcserviceApplication.class, args);
	}
}
