package com.ieadpe.ecbusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcbusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcbusinessApplication.class, args);
	}
}
