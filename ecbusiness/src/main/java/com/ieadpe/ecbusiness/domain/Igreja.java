package com.ieadpe.ecbusiness.domain;

public class Igreja {
	
	private long id;
	private String nome;
	private byte[] foto;
	private TipoTemplo tipoTemplo;
	private Endereco endereco;
	private Pessoa responsavel;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	public TipoTemplo getTipoTemplo() {
		return tipoTemplo;
	}
	public void setTipoTemplo(TipoTemplo tipoTemplo) {
		this.tipoTemplo = tipoTemplo;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Pessoa getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Pessoa responsavel) {
		this.responsavel = responsavel;
	}

}
