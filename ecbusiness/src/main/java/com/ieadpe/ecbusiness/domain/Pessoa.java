package com.ieadpe.ecbusiness.domain;

import java.util.Date;

public class Pessoa {
	
	private long id;
	private String nome;
	private String sobrenome;
	private EstadoCivil estadoCivil;
	private VinculoIgreja vinculo;
	private String cartaoMembro;
	private Date dataNascimento;
	private Date dataBatismo;
	private byte[] foto;
	private Endereco endereco;
	private Igreja congregacao;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public VinculoIgreja getVinculo() {
		return vinculo;
	}
	public void setVinculo(VinculoIgreja vinculo) {
		this.vinculo = vinculo;
	}
	public String getCartaoMembro() {
		return cartaoMembro;
	}
	public void setCartaoMembro(String cartaoMembro) {
		this.cartaoMembro = cartaoMembro;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Date getDataBatismo() {
		return dataBatismo;
	}
	public void setDataBatismo(Date dataBatismo) {
		this.dataBatismo = dataBatismo;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Igreja getCongregacao() {
		return congregacao;
	}
	public void setCongregacao(Igreja congregacao) {
		this.congregacao = congregacao;
	}

}
