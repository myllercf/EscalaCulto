package com.ieadpe.ecbusiness.domain;

public class VinculoIgreja {
	
	private long id;
	private String vinculo;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getVinculo() {
		return vinculo;
	}
	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}

}
