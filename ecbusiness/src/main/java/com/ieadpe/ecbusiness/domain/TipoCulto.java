package com.ieadpe.ecbusiness.domain;

public class TipoCulto {
	
	private long id;
	private String classificacao;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}

}
