package com.ieadpe.ecbusiness.domain;

public class Culto {
	
	private long id;
	private TipoCulto nome;
	private String frequencia;
	private String dia;
	private String turno;
	private Pessoa dirigente;
	private char temDirigente;
	private Igreja igreja;
	private String observacoes;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public TipoCulto getNome() {
		return nome;
	}
	public void setNome(TipoCulto nome) {
		this.nome = nome;
	}
	public String getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(String frequencia) {
		this.frequencia = frequencia;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getTurno() {
		return turno;
	}
	public void setTurno(String turno) {
		this.turno = turno;
	}
	public Pessoa getDirigente() {
		return dirigente;
	}
	public void setDirigente(Pessoa dirigente) {
		this.dirigente = dirigente;
	}
	public char getTemDirigente() {
		return temDirigente;
	}
	public void setTemDirigente(char temDirigente) {
		this.temDirigente = temDirigente;
	}
	public Igreja getIgreja() {
		return igreja;
	}
	public void setIgreja(Igreja igreja) {
		this.igreja = igreja;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

}
