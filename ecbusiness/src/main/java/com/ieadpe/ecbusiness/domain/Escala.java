package com.ieadpe.ecbusiness.domain;

import java.util.Date;

public class Escala {
	
	private long id;
	private Culto culto;
	private Pessoa escalado;
	private Date data;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Culto getCulto() {
		return culto;
	}
	public void setCulto(Culto culto) {
		this.culto = culto;
	}
	public Pessoa getEscalado() {
		return escalado;
	}
	public void setEscalado(Pessoa escalado) {
		this.escalado = escalado;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}

}
