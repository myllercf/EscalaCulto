package com.ieadpe.ecbusiness.exception;

/**
 * Classe de lancamento de excecao de persistencia ocorridos nos DAOs
 * @author myller
 *
 */
public class DaoException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 185509022818327110L;

	/**
	 * Constroe um DAOException com a mensagem informada
	 * @param messagem mensagem da excecao
	 */
	public DaoException(String messagem) {
		super(messagem);
	}
	
	/**
	 * Constroe um DAOException com a causa raiz informada
	 * @param causa excecao lancada 
	 */
	public DaoException(Throwable causa) {
		super(causa);
	}
	
	/**
	 * Constroe um DAOException com a mensagem informada e a causa raiz
	 * @param messagem mensagem da excecao
	 * @param causa excecao lancada 
	 */
	public DaoException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}

}
