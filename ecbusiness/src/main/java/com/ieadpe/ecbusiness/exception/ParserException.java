package com.ieadpe.ecbusiness.exception;

public class ParserException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7189361144247705141L;

	public ParserException (String mensagem) {
		super(mensagem);
	}
	
	public ParserException (Throwable causa) {
		super(causa);
	}
	
	public ParserException (String mensagem, Throwable causa) {
		super(mensagem, causa);
	}

}
